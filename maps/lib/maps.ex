defmodule Maps do
  @moduledoc """
  Documentation for `Maps`.
  """

  @doc """
  The function printKL is used to print the Keyword List
  `[name: "Ada Lovelace", born: 1815, died: 1852, known_for: "mathematics", known_for: "computing" ]`
  using IO.puts/1, Keyword.get_values/2 and Enum.join/2.

  ##Examples

      iex> Maps.printKL
      Ada Lovelace was known for mathematics and computing


  """

  def printKL do
    kl = [name: "Ada Lovelace", born: 1815, died: 1852, known_for: "mathematics", known_for: "computing" ]

    IO.puts("#{Keyword.get_values(kl, :name)} was known for #{Keyword.get_values(kl, :known_for) |> Enum.join(" and ")} " )

  end



  @doc """

  Create a map for representing the following information:

  `Joe Armstrong: Programming Erlang — Software for a Concurrent World (2nd edition), The Pragmatic Bookshelf, 2013.`

  ## Examples

      iex> Maps.createMap
      %{
        author: "Joe Armstrong",
        editorial: "The Pragmatic Bookshelf",
        title: "Programming Erlang ΓÇö Software for a Concurrent World (2nd edition)",
        year: 2013
      }

  """

  def createMap do

    %{author: "Joe Armstrong",
      title: "Programming Erlang — Software for a Concurrent World (2nd edition)" ,
      editorial: "The Pragmatic Bookshelf",
      year: 2013
      }

  end



  @doc """
  This function create a list of maps representing the following information:


  F. Cesarini and S. Thompson: Erlang Programming — A Concurrent Approach to Software Development, O’Reilly, 2009.

  M. Logan, E. Merrit, R. Carlsson: Erlang and OTP in Action, Manning Eds, 2010.

  F. Hébert: Learn you Some Erlang for Great Good!, No Starch Press, 2013.

  Martin Odersky, Lex Spoon, Bill Venners: Programming in Scala, Artima Press 2010.

  M. Fogus: Functional Javascript, O’Reilly, 2013.

  Stuart Halloway, Aaron Bedra: Programming Clojure, The Pragmatic Bookshelf, 2012.

  M. Bevilacqua-Linn: Functional programming Patterns in Scala and Clojure, The Pragmatic Bookshelf, 2013.
  """

  def createListOfMaps do

  [
    %{author: " F. Cesarini and S. Thompson",
    title: "Erlang Programming — A Concurrent Approach to Software Development" ,
    editorial: "O’Reilly",
    year: 2009
    },

    %{author: "M. Logan, E. Merrit, R. Carlsson",
    title: "Erlang and OTP in Action" ,
    editorial: "Manning Eds",
    year: 2010
    },

    %{author: "F. Hébert",
    title: "Learn you Some Erlang for Great Good!" ,
    editorial: "No Starch Press",
    year: 2013
    },


    %{author: "Martin Odersky, Lex Spoon, Bill Venners",
    title: "Programming in Scala" ,
    editorial: "Artima Press",
    year: 2010
    },


    %{author: "M. Fogus",
    title: "Functional Javascript" ,
    editorial: "O’Reilly",
    year: 2013
    } ,

    %{author: "Stuart Halloway, Aaron Bedra",
    title: "Programming Clojure" ,
    editorial: "The Pragmatic Bookshelf",
    year: 2012
    } ,

    %{author: "M. Bevilacqua-Linn",
    title: "Functional programming Patterns in Scala and Clojure" ,
    editorial: "The Pragmatic Bookshelf",
    year: 2013
    }

  ]

  end


  @doc """
  Iterate over the list of maps created on createListOfMaps function and returns only the titles of books published in `year` or earlier are printed.

  ## Examples

      iex(54)> Maps.laterThan(2010)
      ["Erlang and OTP in Action", "Learn you Some Erlang for Great Good!",
      "Programming in Scala", "Functional Javascript", "Programming Clojure",
      "Functional programming Patterns in Scala and Clojure"]
  """

  def laterThan(year) do

    Maps.createListOfMaps
    |> Enum.filter( fn (x) -> x[:year] >= year end)
    |> Enum.map(fn (x) -> x[:title] end)

  end










end
